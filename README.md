# Transit-Core Getting Started

### System Description

This system is primarily designed to archive GTFS-RT data from transit agencies regardless on the amount of variables provided by said agency and maintaining it in a queryable state. Data is archived in real-time and is immediately available to be retrieved by other services down the data pipeline.

It is divided into several modules that allow the user to deploy several scenarios such as:
 * Real-time queryable archiving
 * Real-time long-term archiving
 * Archiving of multiple agencies
 * Test environments with data subsets and no archiving

This system can serve as the foundation for other tools as it allows a GTFS-RT feed to be immediately  replayed without needing to wait on a real-time feed.

While main focus is on GTFS-RT data, GTFS support is also available. All of the data provided by an agency is archived and immediately available to query but not in its original structure. GTFS data is offered as csv files inside a zip file, but some of these are very large to deliver in a single response.

All of the modules are based and deployed via Docker and docker-compose, but Kubernetes support is on the roadmap. A sample configuration for a single-agency archiving has been provided.

### Program Execution

```
docker-compose up
```

### Modules

#### Data API Service

Retrieves GTFS/GTFS-RT information from the database and relays configuration to other modules.

#### Record GTFS-RT Service

Constantly monitors a GTFS-RT feeds of a single agency and archives them.

#### Record GTFS Job

Monitors a GTFS feed and archives a response when there's a change. (Updated at an interval of months).

#### Archive PB Job

Archives GTFS-RT feeds into weekly tar.gz archives for long term storage and dataset sharing.

#### Import PB Archive Job

Imports long-term storage archives into a MongoDB instance. For use in backup/restore and testing environments.

#### Import GTFS Job

Imports a GTFS archive into a MongoDB instance. For use in backup/restore and testing environments.
